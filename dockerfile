FROM node:14.15.4
ENV NODE_ENV=production
WORKDIR /app
COPY package.json /app
RUN npm install --production
COPY . /app
CMD node app.js
EXPOSE 8085