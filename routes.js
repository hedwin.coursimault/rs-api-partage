const express = require('express')
const controller = require('./controller')
const router = express.Router()

router.get('/partage/getPartageCount/post/:idPost', controller.getPartageCount)
router.get('/partage/getPartages', controller.getAllPartage)
router.get('/partage/getPartages/partage/:idPartage', controller.getPartage)
router.get('/partage/getPartages/shareUser/:idUser', controller.getPartageFromUser)
router.get('/partage/getPartages/authorUser/:idUser', controller.getPartageFromAuthor)
router.get('/partage/getPartages/post/:idPost', controller.getPartageFromPost)
router.post('/partage/addPartage', controller.createPartage)
router.put('/partage/emptyPartage/post', controller.emptyPartageFromPost)
router.delete('/partage/deletePartage/partage/:idPartage',controller.removePartage)
router.delete('/partage/deletePartage/user/:idUser',controller.removePartageFromUser)

module.exports = router
