const controller = {}

const Pool = require("pg").Pool;
const pool = new Pool({
    user: 'picarrotAdmin',
    host: '51.159.24.37',
    database: 'db_picarrot_partage',
    password: 'e}KY8x>1Ad?sC_r:BG@n3',
    port: 36144,
})


// Nombre de partages sur un post
// param : id du post
controller.getPartageCount = async (req,res)=> {
    const idPost = req.params.idPost.toString()
    pool.query("SELECT COUNT(*) FROM partage WHERE uuid_post = $1", [idPost] , (error, results) => {
        if (error) {
            res.status(400).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)
        }
       
    })
}

// Renvoie tous les partages
// param : aucun
controller.getAllPartage = async (req,res)=> {
    pool.query('SELECT * FROM partage' , (error, results) => {
        if (error) {
            res.status(401).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)    
        }
        
    })
}

// retourne le partage correspondant
// param : id du partage
controller.getPartage = async (req,res)=> {
    const idPartage = req.params.idPartage.toString()
    pool.query('SELECT * FROM partage WHERE uuid_share = $1', [idPartage], (error, results) => {
    if (error) {
        res.status(401).send("erreur : " + error.message)
    }else{
        res.status(200).json(results.rows)    
    }
   })
}

// Liste des partages qu'un user à fait
// param : id du partage
controller.getPartageFromUser = async (req,res)=> {
    const idUser = req.params.idUser.toString()

    pool.query('SELECT * FROM partage WHERE uuid_share_user = $1',
    [idUser], (error, results) => {
        if (error) {
            res.status(401).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)    
        }
    })
}

// Liste des partages qu'un user à fait
// param : id du partage
controller.getPartageFromAuthor = async (req,res)=> {
    const idUser = req.params.idUser.toString()

    pool.query('SELECT * FROM partage WHERE uuid_author_user = $1',
    [idUser], (error, results) => {
        if (error) {
            res.status(401).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)    
        }
    })
}

// Liste des partages qu'un user à fait
// param : id du partage
controller.getPartageFromPost = async (req,res)=> {
    const idPost = req.params.idPost.toString()

    pool.query('SELECT * FROM partage WHERE uuid_post = $1',
    [idPost], (error, results) => {
        if (error) {
            res.status(401).send("erreur : " + error.message)
        }else{
            res.status(200).json(results.rows)    
        }
    })
}

// Créer un partage
// param : idPost, idShareUser, idAuthorUser
controller.createPartage = async (req,res)=> {
    const { idPost, idShareUser, idAuthorUser } = req.body

    let date_ob = new Date();
    let day = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();

    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    let seconds = date_ob.getSeconds();
    const date = year + "-" + month + "-" + day + " " + hours+":"+minutes+":"+seconds;

    pool.query(
        'INSERT INTO partage(uuid_post, uuid_share_user, uuid_author_user, share_date) VALUES ($1, $2, $3, $4)',
        [idPost,idShareUser,idAuthorUser,date],
        (error, results) => {
            if (error) {
                res.status(401).send("erreur : " + error.message)
            }else{
                res.status(200).send(`Share created`)  
            }
           
        }
    )
}

// Vide les partages d'un post supprimé
// param : idPost
controller.emptyPartageFromPost = async (req,res)=> {
    const { idPost } = req.body

    pool.query(
        'UPDATE partage SET uuid_post = NULL, uuid_author_user = NULL WHERE uuid_post = $1',
        [idPost],
        (error, results) => {
            if (error) {
                res.status(401).send("erreur : " + error.message)
            }else{
                res.status(200).send(`Share emptied`)  
            }
           
        }
    )
}

// Supprime un partage
// param : id du partage à supprimer
controller.removePartage = async (req,res)=> {
    const idPartage = req.params.idPartage.toString()
    pool.query('DELETE FROM "partage" WHERE uuid_share = $1', [idPartage], (error, results) => {
        if (error) {
            res.status(401).send("erreur : " + error.message)
        }else{
            res.status(200).send(`Share deleted with ID: ${idPartage}`)
        }
        
    })
}

// Supprime les partages d'un user
// param : id du user
controller.removePartageFromUser = async (req,res)=> {
    const idUser = req.params.idUser.toString()
    pool.query('DELETE FROM "partage" WHERE uuid_share_user = $1', [idUser], (error, results) => {
        if (error) {
            res.status(401).send("erreur : " + error.message)
        }else{
            res.status(200).send(`Share deleted with ID: ${idPartage}`)
        }
        
    })
}




module.exports = controller
