
const express = require('express')
const app = express()
const routes = require('./routes')
const cors = require('cors');

app.use(cors());

app.use(express.json())
app.use(routes)

// Start server and listen on http://localhost:808*/
var server = app.listen(8085, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("app listening at http://%s:%s", "localhost", port)
});
